/* -*- Mode: C ; c-basic-offset: 2 -*- */
// Copyright (C) 2006-2025 Nedko Arnaudov
// SPDX-License-Identifier: GPL-2.0-only OR ISC

#ifndef RTMEMPOOL_H__1FA54215_11CF_4659_9CF3_C17A10A67A1F__INCLUDED
#define RTMEMPOOL_H__1FA54215_11CF_4659_9CF3_C17A10A67A1F__INCLUDED

void
rtmempool_allocator_init(
  struct lv2_rtsafe_memory_pool_provider * allocator_ptr);

#endif /* #ifndef RTMEMPOOL_H__1FA54215_11CF_4659_9CF3_C17A10A67A1F__INCLUDED */
